package com.mygdx.game.utlis;

public class LinearSpeedTilt implements Tilt {

    private double maxTiltAngle;
    private double tiltSpeed;
    private double returnTiltSpeed;
    private double tiltAngle;


    public LinearSpeedTilt(double maxTiltAngle, double tiltSpeed, double returnTiltSpeed) {
        this.maxTiltAngle = maxTiltAngle;
        this.tiltSpeed = tiltSpeed;
        this.returnTiltSpeed = returnTiltSpeed;
    }

    @Override
    public void tiltToFirstSide() {
        tiltAngle = Math.max(-maxTiltAngle, tiltAngle - tiltSpeed);
    }

    @Override
    public void tiltToSecondSide() {
        tiltAngle = Math.min(maxTiltAngle, tiltAngle + tiltSpeed);
    }

    @Override
    public void resetTilt() {
        tiltAngle = Math.signum(tiltAngle) * Math.max(Math.abs(tiltAngle) - returnTiltSpeed, 0);
    }

    @Override
    public double getTiltAngle() {
        return tiltAngle;
    }
}

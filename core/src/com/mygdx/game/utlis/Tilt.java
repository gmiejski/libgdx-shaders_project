package com.mygdx.game.utlis;

public interface Tilt {
    void tiltToFirstSide();

    void tiltToSecondSide();

    void resetTilt();

    double getTiltAngle();
}

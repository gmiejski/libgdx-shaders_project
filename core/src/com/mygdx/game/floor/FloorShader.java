package com.mygdx.game.floor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g3d.utils.RenderContext;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.GdxRuntimeException;

/**
 * Created by Grzegorz Miejski on 9/8/2014.
 */
public class FloorShader {

    private Mesh mesh;
    private ShaderProgram program;
    private Camera camera;
    private RenderContext renderContext;
    private Texture texture;

    public void init() {
        String vert = Gdx.files.internal("core\\assets\\floor\\vertex.glsl").readString();
        String frag = Gdx.files.internal("core\\assets\\floor\\fragment.glsl").readString();
        program = new ShaderProgram(vert, frag);
        if (!program.isCompiled())
            throw new GdxRuntimeException(program.getLog());

//        texture = new Texture("core\\assets\\floor\\badlogic.jpg");

        mesh = new Mesh(true, 60, 0, new VertexAttribute(VertexAttributes.Usage.Position, 3, ShaderProgram.POSITION_ATTRIBUTE));

        Vector3 v1 = new Vector3(-1000, 0,0);
        Vector3 v2 = new Vector3(1550f, 0,0);
        Vector3 v3 = new Vector3(0.3f, 0,-1500f);

        Vector3 v4 = new Vector3(-1000, 0,0);
        Vector3 v5 = new Vector3(1550f, 0,0);
        Vector3 v6 = new Vector3(0.3f, 0,1500f);

        mesh.setVertices(new float[]{v2.x, v2.y,v2.z, v1.x, v1.y, v1.z, v3.x, v3.y, v3.z,
                v5.x, v5.y,v5.z, v4.x, v4.y, v4.z, v6.x, v6.y, v6.z});

    }

    public void render() {
//        bindTexture();
//        program.setUniformMatrix("u_worldTrans", renderable.worldTransform);

        renderContext.setCullFace(GL20.GL_FRONT);
        mesh.render(program, GL20.GL_TRIANGLES, 0, 6);
        renderContext.setCullFace(GL20.GL_BACK);
        mesh.render(program, GL20.GL_TRIANGLES, 0, 6);
    }

    public void begin(Camera camera, RenderContext context) {
        this.camera = camera;
        this.renderContext = context;
        program.begin();
        program.setUniformMatrix("u_projViewTrans", camera.combined);
        context.setDepthTest(GL20.GL_LEQUAL);

    }

    public void end() {
        program.end();
    }

    public void dispose() {
        program.dispose();
    }

    private void bindTexture() {
        Gdx.graphics.getGL20().glActiveTexture(GL20.GL_TEXTURE0);
        texture.bind(0);
        program.setUniformi("u_texture", 0);
    }
}
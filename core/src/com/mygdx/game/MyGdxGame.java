package com.mygdx.game;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.graphics.g3d.utils.DefaultTextureBinder;
import com.badlogic.gdx.graphics.g3d.utils.RenderContext;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.Bullet;
import com.badlogic.gdx.physics.bullet.collision.*;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.TimeUtils;
import com.mygdx.game.crate.CrateShader;
import com.mygdx.game.explosion.ExplosionShader;
import com.mygdx.game.floor.FloorShader;
import com.mygdx.game.interfaces.MyShader;
import com.mygdx.game.ship.ShipShader;
import com.mygdx.game.skybox.SkyboxShader;

import java.util.*;
import java.util.stream.IntStream;

public class MyGdxGame extends InputAdapter implements ApplicationListener {

    private Camera cam;
    private CameraInputController camController;
    private RenderContext renderContext;
    private Environment environment;

    private ShipShader ship;
    private FloorShader floor;
    private SkyboxShader skyboxShader;
    private List<CrateShader> crates = new ArrayList<>();

    private btCollisionConfiguration collisionConfig;
    private btDispatcher dispatcher;

    private Random random = new Random();

    private Map<Vector3, ExplosionShader> explosions = new HashMap<>();
    private Set<Vector3> previousFrameCollisions = new HashSet<>();
    private Set<Vector3> currentFrameCollisions = new HashSet<>();

    private SpriteBatch batch;
    private BitmapFont font;

    private Sound crateHitSound;

    @Override
    public void create() {
        setupEnvironment();
        setupCamera();
        Bullet.init();
        setupTextRenderer();

        collisionConfig = new btDefaultCollisionConfiguration();
        dispatcher = new btCollisionDispatcher(collisionConfig);

        renderContext = new RenderContext(new DefaultTextureBinder(DefaultTextureBinder.WEIGHTED, 1));
        ship = new ShipShader();
        ship.init(environment);

//        floor = new FloorShader();
        //      floor.init();
        skyboxShader = new SkyboxShader();
        skyboxShader.init();

        IntStream.range(0, 100).forEach(
                number -> crates.add(new CrateShader(new Vector3(random.nextInt(100) - 50, random.nextInt(100) - 50, random.nextInt(100) - 50)))
        );
        crates.forEach(MyShader::init);

        crateHitSound = Gdx.audio.newSound(Gdx.files.internal("core\\assets\\sounds\\crateHit.mp3"));

    }

    private void setupTextRenderer() {
        batch = new SpriteBatch();
        font = new BitmapFont();
        font.setColor(Color.RED);
        font.scale(3);
    }


    @Override
    public void render() {

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        if (false) {
            cam = new OrthographicCamera();
            ((OrthographicCamera) cam).setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            cam.update();
            batch.begin();
            font.draw(batch, "GAME OVER!", Gdx.graphics.getWidth() / 4, Gdx.graphics.getHeight() / 2);
            batch.flush();
            batch.end();
            batch.flush();
        } else {
            currentFrameCollisions.clear();
            cam.lookAt(ship.getPosition().cpy());
            cam.update();
            camController.update();
            Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

            ship.begin(cam, renderContext);
            ship.render();
            ship.end();

            skyboxShader.begin(cam, renderContext);
            skyboxShader.render();
            skyboxShader.end();
//            floor.begin(cam, renderContext);
//            floor.render();
//            floor.end();

            drawBalls();

            drawCrates();
            drawExplosions();

            previousFrameCollisions.clear();
            previousFrameCollisions.addAll(currentFrameCollisions);
        }
    }

    private boolean gameEnded() {
        return ship.isErased() && timeFromShipDestoy() > 3000;
    }

    private long timeFromShipDestoy() {
        return TimeUtils.millis() - ship.getEraseMoment();
    }

    private void drawBalls() {
        for (MyShader ball : ship.getBalls()) {
            ball.begin(cam, renderContext);
            ball.render();
            ball.end();

            if (!ball.isErased()) {
                crates.stream().filter(crate -> !crate.dead())
                        .filter(crate -> crate.getPosition().dst(ball.getPosition()) < 2.0f)
                        .filter(crate -> checkCollision(ball.getCollisionObject(), crate.getCollisionObject()))
                        .forEach(crate -> {
                            crateHitSound.play();
                            crate.hit();
                            ball.markErased();
                            System.out.println("Collision with ball!\n");
                            currentFrameCollisions.add(ball.getPosition());
                        });
            }
        }

        ship.getBalls().stream()
                .filter(Objects::nonNull)
                .filter(MyShader::isErased)
                .forEach(Disposable::dispose);

        ship.getBalls().removeIf(MyShader::isErased);
    }


    private void drawCrates() {
        for (CrateShader crate : crates) {
            crate.begin(cam, renderContext);
            crate.render();
            crate.end();

            if (!crate.dead() && !ship.isErased() && checkCollision(ship.getCollisionObject(), crate.getCollisionObject())) {
                crateHitSound.play();
                currentFrameCollisions.add(crate.getPosition());
                ship.markErased();
            }
        }
    }

    private void drawExplosions() {
        currentFrameCollisions.forEach(v -> {
            if (!previousFrameCollisions.contains(v) && !explosions.containsKey(v)) {
                ExplosionShader explosionShader = new ExplosionShader(v);
                explosions.put(v, explosionShader);
                explosionShader.init();
            }
        });

        Set<Vector3> toRemove = new HashSet<>();
        explosions.forEach((k, v) -> {
            v.begin(cam, renderContext);
            v.render();
            v.end();

            if (v.isErased()) {
                toRemove.add(k);
            }
        });

        toRemove.stream()
                .map(explosions::remove)
                .map(ExplosionShader.class::cast)
                .filter(Objects::nonNull)
                .forEach(ExplosionShader::dispose);
    }

    @Override
    public void dispose() {
        ship.dispose();
        crates.forEach(MyShader::dispose);
        explosions.values().forEach(MyShader::dispose);
        dispatcher.dispose();
        collisionConfig.dispose();
        batch.dispose();
        font.dispose();
    }

    @Override
    public void resume() {
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {
    }

    private void setupEnvironment() {
        environment = new Environment();
        environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
        environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));
    }

    private void setupCamera() {
        cam = new PerspectiveCamera(67, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        cam.position.set(0f, 10f, 10f);
        cam.lookAt(0, 0, 0);
        cam.near = 1f;
        cam.far = 300f;
        cam.update();

        camController = new CameraInputController(cam);
        Gdx.input.setInputProcessor(camController);
    }

    boolean checkCollision(btCollisionObject collisionObject1, btCollisionObject collisionObject2) {
        CollisionObjectWrapper co0 = new CollisionObjectWrapper(collisionObject1);
        CollisionObjectWrapper co1 = new CollisionObjectWrapper(collisionObject2);

        btCollisionAlgorithmConstructionInfo ci = new btCollisionAlgorithmConstructionInfo();
        ci.setDispatcher1(dispatcher);
        btCollisionAlgorithm algorithm = new btSphereBoxCollisionAlgorithm(null, ci, co0.wrapper, co1.wrapper, false);

        btDispatcherInfo info = new btDispatcherInfo();
        btManifoldResult result = new btManifoldResult(co0.wrapper, co1.wrapper);

        algorithm.processCollision(co0.wrapper, co1.wrapper, info, result);

        boolean collisionOccured = result.getPersistentManifold().getNumContacts() > 0;

        result.dispose();
        info.dispose();
        algorithm.dispose();
        ci.dispose();
        co1.dispose();
        co0.dispose();

        return collisionOccured;
    }
}

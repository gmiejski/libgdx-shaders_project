package com.mygdx.game.explosion;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.loaders.ModelLoader;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.Shader;
import com.badlogic.gdx.graphics.g3d.loader.ObjLoader;
import com.badlogic.gdx.graphics.g3d.model.NodePart;
import com.badlogic.gdx.graphics.g3d.utils.RenderContext;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.collision.btBoxShape;
import com.badlogic.gdx.physics.bullet.collision.btCollisionObject;
import com.badlogic.gdx.physics.bullet.collision.btCollisionShape;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.mygdx.game.interfaces.MyShader;

/**
 * Created by Grzegorz Miejski (sg0221133) on 9/11/2014.
 */
public class ExplosionShader implements MyShader {

    private ShaderProgram program;

    private Model model;
    private Texture texture;
    private Renderable explosionRenderable;

    private Vector3 position;

    private btCollisionShape explosionShape;
    private btCollisionObject explosionObject;
    private boolean erased;
    private float scale = 1f;
    private float fadeOut = 1f;

    public ExplosionShader(Vector3 position) {
        this.position = position.cpy();
    }

    @Override
    public void init(Environment environment) {
        init();
        explosionRenderable.environment = environment;
    }

    @Override
    public Vector3 getPosition() {
        return position;
    }

    @Override
    public void markErased() {
        erased = true;
    }

    @Override
    public boolean isErased() {
        return erased;
    }

    @Override
    public void init() {

        ModelLoader modelLoader2 = new ObjLoader();
        model = modelLoader2.loadModel(Gdx.files.internal("core\\assets\\explosion\\explosion.obj"));

        NodePart explosionBlockPart = model.nodes.get(0).parts.get(0);

        explosionRenderable = new Renderable();
        explosionBlockPart.setRenderable(explosionRenderable);
        explosionRenderable.worldTransform.idt();

        String vert = Gdx.files.internal("core\\assets\\explosion\\vertex.glsl").readString();
        String frag = Gdx.files.internal("core\\assets\\explosion\\fragment.glsl").readString();
        program = new ShaderProgram(vert, frag);
        if (!program.isCompiled())
            throw new GdxRuntimeException(program.getLog());

        texture = new Texture("core\\assets\\explosion\\explosion.png");

        explosionShape = new btBoxShape(new Vector3(1, 0.55f, 1));
        explosionObject = new btCollisionObject();
        explosionObject.setCollisionShape(explosionShape);
        explosionObject.setWorldTransform(newTranslationScaleMatrix());
    }

    @Override
    public void begin(Camera camera, RenderContext context) {
        program.begin();
        program.setUniformMatrix("u_projViewTrans", camera.combined);
        program.setUniformMatrix("u_moveMatrix", newTranslationScaleMatrix());
        context.setDepthTest(GL20.GL_LEQUAL);
        context.setCullFace(GL20.GL_BACK);
    }

    @Override
    public void render(Renderable renderable) {
        bindTexture();
        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

        scale += 0.55f;
        if (scale >= 12f) {
            markErased();
        }
        fadeOut -= 0.04f;
        program.setUniformMatrix("u_worldTrans", renderable.worldTransform);
        program.setUniformf("u_fadeOut", fadeOut);
        renderable.mesh.render(program,
                renderable.primitiveType,
                renderable.meshPartOffset,
                renderable.meshPartSize);

        Gdx.gl.glDisable(GL20.GL_BLEND);
    }

    @Override
    public void end() {
        program.end();
    }

    private void bindTexture() {
        Gdx.graphics.getGL20().glActiveTexture(GL20.GL_TEXTURE1);
        texture.bind(1);
        program.setUniformi("u_texture", 1);
    }

    @Override
    public int compareTo(Shader other) {
        return 0;
    }

    @Override
    public boolean canRender(Renderable instance) {
        return false;
    }

    @Override
    public void dispose() {
        program.dispose();
        model.dispose();
    }

    private Matrix4 newTranslationScaleMatrix() {
        return new Matrix4(new float[]{
                scale, 0f, 0f, 0f,
                0f, scale, 0f, 0f,
                0f, 0f, scale, 0f,
                position.x, position.y, position.z, 1f});
    }

    @Override
    public btCollisionObject getCollisionObject() {
        return explosionObject;
    }

    @Override
    public void render() {
        render(explosionRenderable);
    }
}

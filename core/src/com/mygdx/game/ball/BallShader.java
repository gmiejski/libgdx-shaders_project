package com.mygdx.game.ball;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.Shader;
import com.badlogic.gdx.graphics.g3d.utils.RenderContext;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.collision.btBoxShape;
import com.badlogic.gdx.physics.bullet.collision.btCollisionObject;
import com.badlogic.gdx.physics.bullet.collision.btCollisionShape;
import com.badlogic.gdx.utils.TimeUtils;
import com.mygdx.game.interfaces.MyShader;
import com.mygdx.game.utlis.ShaderLoader;

import java.util.Random;

import static java.lang.Math.cos;
import static java.lang.Math.sin;

/**
 * Created by Grzegorz Miejski (sg0221133) on 9/11/2014.
 */
public class BallShader implements MyShader {

    public static final float PIRAMID_PEAK = 0.6f;
    public static final Vector3 PYRAMID_TOP = new Vector3(0f, PIRAMID_PEAK, 0f);
    public static final float EDGE_SIZE = 0.3f;
    private final double rotSpeedY;
    private final double rotSpeedX;
    private final double rotSpeedZ;
    private ShaderProgram shaderProgram;
    private Mesh mesh;

    private Camera camera;
    private RenderContext renderContext;

    private Vector3 position;

    private btCollisionShape ballShape;
    private btCollisionObject ballObject;

    private double rot;
    private Vector2 speed = new Vector2(0.2f, 0.2f);
    private boolean erased = false;
    private long startTime;

    {
        mesh = new Mesh(true, 60, 0,
                new VertexAttribute(VertexAttributes.Usage.Position, 3, ShaderProgram.POSITION_ATTRIBUTE),
                new VertexAttribute(VertexAttributes.Usage.Color, 3, ShaderProgram.COLOR_ATTRIBUTE));

        Vector3 v1_1 = new Vector3(-EDGE_SIZE, 0, EDGE_SIZE);
        Vector3 v1_2 = new Vector3(EDGE_SIZE, 0, EDGE_SIZE);

        Vector3 v2_1 = new Vector3(-EDGE_SIZE, 0, -EDGE_SIZE);
        Vector3 v2_2 = new Vector3(-EDGE_SIZE, 0, EDGE_SIZE);

        Vector3 v3_1 = new Vector3(EDGE_SIZE, 0, -EDGE_SIZE);
        Vector3 v3_2 = new Vector3(-EDGE_SIZE, 0, -EDGE_SIZE);

        Vector3 v4_1 = new Vector3(EDGE_SIZE, 0, EDGE_SIZE);
        Vector3 v4_2 = new Vector3(EDGE_SIZE, 0, -EDGE_SIZE);

        Vector3 v5_1 = new Vector3(-EDGE_SIZE, 0, EDGE_SIZE);
        Vector3 v5_2 = new Vector3(-EDGE_SIZE, 0, -EDGE_SIZE);
        Vector3 v5_3 = new Vector3(EDGE_SIZE, 0, -EDGE_SIZE);

        Vector3 v6_1 = new Vector3(-EDGE_SIZE, 0, EDGE_SIZE);
        Vector3 v6_2 = new Vector3(EDGE_SIZE, 0, -EDGE_SIZE);
        Vector3 v6_3 = new Vector3(EDGE_SIZE, 0, EDGE_SIZE);

        mesh.setVertices(new float[]{
                v5_1.x, v5_1.y, v5_1.z, 0.3f, 0.3f, 0.3f,
                v5_2.x, v5_2.y, v5_2.z, 0.3f, 0.3f, 0.3f,
                v5_3.x, v5_3.y, v5_3.z, 0.3f, 0.3f, 0.3f,
                v6_1.x, v6_1.y, v6_1.z, 0.3f, 0.3f, 0.3f,
                v6_2.x, v6_2.y, v6_2.z, 0.3f, 0.3f, 0.3f,
                v6_3.x, v6_3.y, v6_3.z, 0.3f, 0.3f, 0.3f,
                v1_1.x, v1_1.y, v1_1.z, 1, 0, 0,
                v1_2.x, v1_2.y, v1_2.z, 1, 0, 0,
                PYRAMID_TOP.x, PYRAMID_TOP.y, PYRAMID_TOP.z, 1, 0, 0,
                v2_1.x, v2_1.y, v2_1.z, 0, 1, 0,
                v2_2.x, v2_2.y, v2_2.z, 0, 1, 0,
                PYRAMID_TOP.x, PYRAMID_TOP.y, PYRAMID_TOP.z, 0, 1, 0,
                v3_1.x, v3_1.y, v3_1.z, 0, 0, 1,
                v3_2.x, v3_2.y, v3_2.z, 0, 0, 1,
                PYRAMID_TOP.x, PYRAMID_TOP.y, PYRAMID_TOP.z, 0, 0, 1,
                v4_1.x, v4_1.y, v4_1.z, 1, 0, 1,
                v4_2.x, v4_2.y, v4_2.z, 1, 0, 1,
                PYRAMID_TOP.x, PYRAMID_TOP.y, PYRAMID_TOP.z, 1, 0, 1});
    }

    private double xRot;
    private double yRot;

    public BallShader(Vector3 position, double rot) {
        this.rot = rot;
        this.position = new Vector3(position.cpy());
        xRot = Math.random() / 3;
        yRot = Math.random() / 3;
        rotSpeedX = Math.random() * (0.18f) - 0.09f;
        rotSpeedY = Math.random() * (0.18f) - 0.09f;
        rotSpeedZ = Math.random() * (0.18f) - 0.09f;
    }

    @Override
    public void init(Environment environment) {
        init();
    }

    @Override
    public Vector3 getPosition() {
        return position;
    }

    @Override
    public void markErased() {
        erased = true;
    }

    @Override
    public boolean isErased() {
        return erased;
    }

    @Override
    public void init() {
        shaderProgram = ShaderLoader.createShader("core\\assets\\ball\\vertex.glsl", "core\\assets\\ball\\fragment.glsl");
        startTime = TimeUtils.millis();
        ballShape = new btBoxShape(new Vector3(0.6f, 0.6f, 0.6f));
        ballObject = new btCollisionObject();
        ballObject.setCollisionShape(ballShape);

    }

    @Override
    public void begin(Camera camera, RenderContext context) {
        this.camera = camera;
        this.renderContext = context;

        moveBall();

        shaderProgram.begin();
        shaderProgram.setUniformMatrix("u_projViewTrans", camera.combined);
        shaderProgram.setUniformMatrix("u_moveMatrix", newTranslationMatrix());
        shaderProgram.setUniformMatrix("u_rotationMatrix", rotationMatrix());
        context.setDepthTest(GL20.GL_LEQUAL);
        context.setCullFace(GL20.GL_BACK);
    }

    private Matrix4 rotationMatrix() {
        return newRotationMatrix_X().mul(newRotationMatrix_Y());
    }

    private void moveBall() {
        position.z += speed.y * cos(rot);
        position.x -= speed.x * sin(rot);
    }

    @Override
    public void render(Renderable renderable) {
        if (TimeUtils.timeSinceMillis(startTime) > 6000) {
            markErased();
        }

        updateRotation();

        ballObject.setWorldTransform(newTranslationMatrix());

        Gdx.gl.glEnable(GL20.GL_CULL_FACE);
        Gdx.gl.glCullFace(GL20.GL_BACK);
        Gdx.gl.glFrontFace(GL20.GL_CCW);

        mesh.render(shaderProgram, GL20.GL_TRIANGLES, 0, 18);
    }

    private void updateRotation() {
        xRot += rotSpeedX;
        yRot += rotSpeedY;
    }

    @Override
    public void end() {
        shaderProgram.end();
    }

    @Override
    public int compareTo(Shader other) {
        return 0;
    }

    @Override
    public boolean canRender(Renderable instance) {
        return true;
    }

    @Override
    public void dispose() {
        shaderProgram.dispose();
    }


    private Matrix4 newTranslationMatrix() {
        return new Matrix4(new float[]{
                1f, 0f, 0f, 0f,
                0f, 1f, 0f, 0f,
                0f, 0f, 1f, 0f,
                position.x, position.y, position.z, 1f});
    }

    private Matrix4 newRotationMatrix_X() {
        return new Matrix4(new float[]{
                1f, 0f, 0f, 0f,
                0f, (float) cos(xRot), -(float) sin(xRot), 0f,
                0f, (float) sin(xRot), (float) cos(xRot), 0f,
                0f, 0f, 0f, 1f});
    }

    private Matrix4 newRotationMatrix_Y() {
        return new Matrix4(new float[]{
                (float) cos(yRot), 0f, (float) sin(yRot), 0f,
                0f, 1f, 0f, 0f,
                (float) -sin(yRot), 0f, (float) cos(yRot), 0f,
                0f, 0f, 0f, 1f});
    }

    private Matrix4 newRotationMatrix_Z() {
        return new Matrix4(new float[]{
                (float) cos(yRot), (float) -sin(yRot), 0f, 0f,
                (float) sin(yRot), (float) cos(yRot), 0f, 0f,
                0f, 1f, 0f, 0f,
                0f, 0f, 0f, 1f});
    }


    @Override
    public btCollisionObject getCollisionObject() {
        return ballObject;
    }

    @Override
    public void render() {
        render(null);
    }

}

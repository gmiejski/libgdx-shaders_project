package com.mygdx.game.interfaces;

import com.badlogic.gdx.physics.bullet.collision.btCollisionObject;

/**
 * Created by Grzegorz Miejski on 9/10/2014.
 */
public interface Collisionable {

    public btCollisionObject getCollisionObject();

}

package com.mygdx.game.interfaces;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Shader;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by Grzegorz Miejski on 9/10/2014.
 */
public interface MyShader extends Shader, Collisionable {


    void render();

    void init(Environment environment);

    Vector3 getPosition();

    void markErased();

    boolean isErased();
}

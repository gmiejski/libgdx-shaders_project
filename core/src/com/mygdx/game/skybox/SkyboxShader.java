package com.mygdx.game.skybox;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.loaders.ModelLoader;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.Shader;
import com.badlogic.gdx.graphics.g3d.loader.ObjLoader;
import com.badlogic.gdx.graphics.g3d.model.NodePart;
import com.badlogic.gdx.graphics.g3d.utils.RenderContext;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.collision.btBoxShape;
import com.badlogic.gdx.physics.bullet.collision.btCollisionObject;
import com.badlogic.gdx.physics.bullet.collision.btCollisionShape;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.mygdx.game.interfaces.MyShader;

/**
 * Created by Grzegorz Miejski on 9/9/2014.
 */
public class SkyboxShader implements MyShader {

    private ShaderProgram program;
    private Camera camera;
    private RenderContext renderContext;

    private Model model;
    private Texture texture;
    private Renderable crateRenderable;

    private Vector3 position = new Vector3(0f, 0f, 0f);

    private btCollisionShape crateShape;
    private btCollisionObject crateObject;
    private boolean erased;


    @Override
    public void init(Environment environment) {
        init();
        crateRenderable.environment = environment;
    }

    @Override
    public Vector3 getPosition() {
        return position;
    }

    @Override
    public void markErased() {
        erased = true;
    }

    @Override
    public boolean isErased() {
        return erased;
    }

    @Override
    public void init() {

        ModelLoader modelLoader2 = new ObjLoader();
        model = modelLoader2.loadModel(Gdx.files.internal("core\\assets\\skybox\\skybox.obj"));
        model.meshes.first().scale(150, 150, 150);

        NodePart crateBlockPart = model.nodes.get(0).parts.get(0);

        crateRenderable = new Renderable();
        crateBlockPart.setRenderable(crateRenderable);
        crateRenderable.worldTransform.idt();

        String vert = Gdx.files.internal("core\\assets\\skybox\\vertex.glsl").readString();
        String frag = Gdx.files.internal("core\\assets\\skybox\\fragment.glsl").readString();
        program = new ShaderProgram(vert, frag);
        if (!program.isCompiled())
            throw new GdxRuntimeException(program.getLog());

        texture = new Texture("core\\assets\\skybox\\skybox.jpg");

        crateShape = new btBoxShape(new Vector3(1, 0.55f, 1));
        crateObject = new btCollisionObject();
        crateObject.setCollisionShape(crateShape);
        crateObject.setWorldTransform(newTranslationMatrix());
    }

    @Override
    public void begin(Camera camera, RenderContext context) {

        this.camera = camera;
        this.renderContext = context;
        program.begin();
        program.setUniformMatrix("u_projViewTrans", camera.combined);
        program.setUniformMatrix("u_moveMatrix", newTranslationMatrix());
//        program.setUniform3fv("u_LightPos", LIGHT_POS, 0, 3);
        context.setDepthTest(GL20.GL_LEQUAL);
        context.setCullFace(GL20.GL_BACK);
    }

    @Override
    public void render(Renderable renderable) {
        bindTexture();


        program.setUniformMatrix("u_worldTrans", renderable.worldTransform);
        renderable.mesh.render(program,
                renderable.primitiveType,
                renderable.meshPartOffset,
                renderable.meshPartSize);
    }

    @Override
    public void end() {
        program.end();
    }

    private void bindTexture() {
        Gdx.graphics.getGL20().glActiveTexture(GL20.GL_TEXTURE1);
        texture.bind(1);
        program.setUniformi("u_texture", 1);
    }

    @Override
    public int compareTo(Shader other) {
        return 0;
    }

    @Override
    public boolean canRender(Renderable instance) {
        return false;
    }

    @Override
    public void dispose() {
        program.dispose();
        model.dispose();
    }

    private Matrix4 newTranslationMatrix() {
        return new Matrix4(new float[]{
                1f, 0f, 0f, 0f,
                0f, 1f, 0f, 0f,
                0f, 0f, 1f, 0f,
                position.x, position.y, position.z, 1f});
    }

    @Override
    public btCollisionObject getCollisionObject() {
        return crateObject;
    }

    @Override
    public void render() {
        render(crateRenderable);
    }
}

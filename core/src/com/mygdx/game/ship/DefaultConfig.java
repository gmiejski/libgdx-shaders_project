package com.mygdx.game.ship;

/**
 * Created by Grzegorz Miejski on 9/9/2014.
 */
public class DefaultConfig {

    public static final float MIN_CAMERA_HEIGHT = 5f;

    public boolean cameraLocked = false;
    public float cameraHeight = 10.0f;

    public void cameraUp() {
        cameraHeight += 0.5f;
    }

    public void cameraDown() {
        cameraHeight -= 0.5f;
        if (cameraHeight < MIN_CAMERA_HEIGHT) {
            cameraHeight = MIN_CAMERA_HEIGHT;
        }
    }
}

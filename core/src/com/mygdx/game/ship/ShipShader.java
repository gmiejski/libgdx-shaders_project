package com.mygdx.game.ship;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.loaders.ModelLoader;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.Shader;
import com.badlogic.gdx.graphics.g3d.loader.ObjLoader;
import com.badlogic.gdx.graphics.g3d.model.NodePart;
import com.badlogic.gdx.graphics.g3d.utils.RenderContext;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.collision.btBoxShape;
import com.badlogic.gdx.physics.bullet.collision.btCollisionObject;
import com.badlogic.gdx.physics.bullet.collision.btCollisionShape;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.TimeUtils;
import com.mygdx.game.ball.BallShader;
import com.mygdx.game.interfaces.MyShader;
import com.mygdx.game.utlis.LinearSpeedTilt;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.badlogic.gdx.Gdx.input;
import static com.badlogic.gdx.Input.Buttons;
import static com.badlogic.gdx.Input.Keys;
import static java.lang.Math.cos;
import static java.lang.Math.sin;

/**
 * @author Grzegorz Miejski
 *         on 2014-09-07.
 */
public class ShipShader implements MyShader {

    public static final float MIN_HEIGHT = -105.5f;
    private static final double ROT_SPEED = 0.04f;
    private static final double TILT_ANGLE = 0.6f;
    private static final double TILT_SPEED = 0.01f;
    private static final double RETURN_TILT_SPEED = 0.03f;
    private static final Vector3 LEFT_RIGHT_TILT_VECTOR = new Vector3(0.0f, 0.0f, 1.0f);
    private static final Vector3 UP_DOWN_TILT_VECTOR = new Vector3(1.0f, 0.0f, 0.0f);
    private static final float[] LIGHT_POS = new float[]{0.4f, 0.4f, 0.4f};
    private ShaderProgram program;
    private Camera camera;

    private Model model;
    private Texture texture;
    private Renderable shipRenderable;

    private Vector3 position = Vector3.Zero;
    private Vector2 speed = new Vector2(0.07f, 0.07f);
    private double rot = 0f;

    private DefaultConfig config = new DefaultConfig();

    private btCollisionObject shipObject;

    private List<MyShader> balls = new ArrayList<>();
    private boolean erased = false;

    private LinearSpeedTilt leftRightTilt;
    private LinearSpeedTilt upDownTilt;
    private long eraseMoment;
    private long lastShot;

    private Sound bulletFiredSound;

    @Override
    public void init(Environment environment) {
        init();
        shipRenderable.environment = environment;
    }

    @Override
    public Vector3 getPosition() {
        return position;
    }

    @Override
    public void markErased() {
        if (!erased) {
            erased = true;
            eraseMoment = TimeUtils.millis();
        }
    }

    @Override
    public boolean isErased() {
        return erased;
    }

    @Override
    public void init() {
        ModelLoader modelLoader = new ObjLoader();
        model = modelLoader.loadModel(Gdx.files.internal("core\\assets\\ship\\ship.obj"));

        NodePart shipBlockPart = model.nodes.get(0).parts.get(0);

        shipRenderable = new Renderable();
        shipBlockPart.setRenderable(shipRenderable);

        shipRenderable.worldTransform.idt();

        String vert = Gdx.files.internal("core\\assets\\ship\\vertex.glsl").readString();
        String frag = Gdx.files.internal("core\\assets\\ship\\fragment.glsl").readString();
        program = new ShaderProgram(vert, frag);
        if (!program.isCompiled())
            throw new GdxRuntimeException(program.getLog());

        texture = new Texture("core\\assets\\ship\\ship.png");

        btCollisionShape shipShape = new btBoxShape(new Vector3(0.6f, 0f, 0.6f));
        shipObject = new btCollisionObject();
        shipObject.setCollisionShape(shipShape);

        position.y = 0f;

        leftRightTilt = new LinearSpeedTilt(TILT_ANGLE, TILT_SPEED, RETURN_TILT_SPEED);
        upDownTilt = new LinearSpeedTilt(TILT_ANGLE, TILT_SPEED, RETURN_TILT_SPEED);

        bulletFiredSound = Gdx.audio.newSound(Gdx.files.internal("core\\assets\\sounds\\bulletFired.mp3"));
    }

    @Override
    public void begin(Camera camera, RenderContext context) {
        readConfigKeys();

        this.camera = camera;
        program.begin();
        program.setUniformMatrix("u_projViewTrans", camera.combined);
        program.setUniformMatrix("u_moveMatrix", newTranslationMatrix());
        program.setUniformMatrix("u_rotation", newRotationMatrix());
        program.setUniform3fv("u_LightPos", LIGHT_POS, 0, 3);
        context.setDepthTest(GL20.GL_LEQUAL);
        context.setCullFace(GL20.GL_BACK);
    }

    @Override
    public void render(Renderable renderable) {
        bindTexture();
        shipObject.setWorldTransform(newTranslationMatrix().mul(newRotationMatrix_Y()));

        if (!erased) {
            program.setUniformMatrix("u_worldTrans", renderable.worldTransform);
            renderable.mesh.render(program,
                    renderable.primitiveType,
                    renderable.meshPartOffset,
                    renderable.meshPartSize);

            updateCamera();
        }
        deleteFarBalls();
    }

    private void deleteFarBalls() {
        balls.forEach(myShader -> {
            if (myShader.getPosition().dst(new Vector3()) > 200) {
                myShader.markErased();
                myShader.dispose();
            }
        });

        balls = balls.stream().filter(ball -> !ball.isErased()).collect(Collectors.toList());
    }

    @Override
    public void end() {
        program.end();
    }

    private void readConfigKeys() {
        if (input.isKeyJustPressed(Keys.L)) {
            config.cameraLocked = !config.cameraLocked;
        }
        if (config.cameraLocked) {
            if (input.isKeyPressed(Keys.PAGE_UP)) {
                config.cameraUp();
            }

            if (input.isKeyPressed(Keys.PAGE_DOWN)) {
                config.cameraDown();
            }
        }

        if (input.isKeyJustPressed(Keys.R)) {
            resetGame();
        }

    }

    private void resetGame() {
        erased = false;
        position = new Vector3();
        camera.position.set(0, 10, 10);
        camera.lookAt(0, 0, 0);
        camera.update();
    }

    private void bindTexture() {
        Gdx.graphics.getGL20().glActiveTexture(GL20.GL_TEXTURE0);
        texture.bind(0);
        program.setUniformi("u_texture", 0);
    }

    private Matrix4 newRotationMatrix() {
        return newRotationMatrix_U(UP_DOWN_TILT_VECTOR, upDownTilt.getTiltAngle())
                .mulLeft(newRotationMatrix_U(LEFT_RIGHT_TILT_VECTOR, leftRightTilt.getTiltAngle())
                        .mulLeft(newRotationMatrix_Y()));
    }

    private Matrix4 newRotationMatrix_Y() {
        return new Matrix4(new float[]{
                (float) cos(rot), 0f, (float) sin(rot), 0f,
                0f, 1f, 0f, 0f,
                (float) -sin(rot), 0f, (float) cos(rot), 0f,
                0f, 0f, 0f, 1f});
    }

    private Matrix4 newRotationMatrix_U(Vector3 u, double theta) {
        float cosT = (float) cos(theta);
        float sinT = (float) sin(theta);

        float m00 = cosT + u.x * u.x * (1 - cosT);
        float m01 = u.x * u.y * (1 - cosT) - u.z * sinT;
        float m02 = u.x * u.z * (1 - cosT) + u.y * sinT;
        float m10 = u.y * u.x * (1 - cosT) + u.z * sinT;
        float m11 = cosT + u.y * u.y * (1 - cosT);
        float m12 = u.y * u.x * (1 - cosT) - u.x * sinT;
        float m20 = u.z * u.x * (1 - cosT) - u.y * sinT;
        float m21 = u.z * u.y * (1 - cosT) + u.x * sinT;
        float m22 = cosT + u.z * u.z * (1 - cosT);

        return new Matrix4(new float[]{
                m00, m01, m02, 0f,
                m10, m11, m12, 0f,
                m20, m21, m22, 0f,
                0f, 0f, 0f, 1f});
    }

    private Matrix4 newTranslationMatrix() {
        if (!erased) {
            if (input.isKeyPressed(Keys.Z)) {
                position.y += speed.y;
                upDownTilt.tiltToSecondSide();
            } else if (input.isKeyPressed(Keys.X)) {
                position.y -= speed.y;
                if (position.y < MIN_HEIGHT) {
                    position.y = MIN_HEIGHT;
                }
                upDownTilt.tiltToFirstSide();
            } else {
                upDownTilt.resetTilt();
            }

            if (input.isKeyPressed(Keys.RIGHT)) {
                rot += ROT_SPEED;
                leftRightTilt.tiltToFirstSide();
            } else if (input.isKeyPressed(Keys.LEFT)) {
                rot -= ROT_SPEED;
                leftRightTilt.tiltToSecondSide();
            } else {
                leftRightTilt.resetTilt();
            }

            if (input.isKeyPressed(Keys.UP)) {
                position.z += speed.y * cos(rot);
                position.x -= speed.x * sin(rot);
            } else if (input.isKeyPressed(Keys.DOWN)) {
                position.z -= speed.y * cos(rot);
                position.x += speed.x * sin(rot);
            }

            if (input.isKeyPressed(Keys.SPACE)) {
                resetPosition();
            }

            if (input.isButtonPressed(Buttons.RIGHT) || input.isKeyPressed(Keys.CONTROL_LEFT)) {
                if (TimeUtils.timeSinceMillis(lastShot) > 600) {
                    lastShot = TimeUtils.millis();
                    BallShader newBall = new BallShader(position, rot);
                    newBall.init();
                    balls.add(newBall);
                    bulletFiredSound.play();
                }
            }
        }

        return new Matrix4(new float[]{
                1f, 0f, 0f, 0f,
                0f, 1f, 0f, 0f,
                0f, 0f, 1f, 0f,
                position.x, position.y, position.z, 1f});
    }

    private void updateCamera() {
        if (config.cameraLocked) {
            camera.position.set(position.x, position.y + config.cameraHeight, position.z);
            camera.lookAt(position);
            camera.update();
        }
    }

    private void resetPosition() {
        position.x = 0f;
        position.y = 0f;
    }

    @Override
    public int compareTo(Shader other) {
        return 0;
    }

    @Override
    public boolean canRender(Renderable instance) {
        return true;
    }

    @Override
    public void dispose() {
        program.dispose();
        model.dispose();
    }

    @Override
    public btCollisionObject getCollisionObject() {
        return shipObject;
    }

    @Override
    public void render() {
        render(shipRenderable);
    }


    public List<MyShader> getBalls() {
        return balls;
    }

    public long getEraseMoment() {
        return eraseMoment;
    }
}

#ifdef GL_ES 
precision mediump float;
#endif

uniform vec3 u_LightPos;
uniform sampler2D u_texture;

varying vec3 v_Position;
varying vec3 v_Normal;
varying vec2 v_texCoord0;

void main() {
    float distance = length(u_LightPos - v_Position);
    vec3 lightVector = normalize(u_LightPos - v_Position);

    float diffuse = max(dot(v_Normal, lightVector), 0.3);
    diffuse = diffuse * (1.0 / (1.0 + (0.25 * distance * distance)));

    gl_FragColor = texture2D(u_texture, v_texCoord0) * diffuse;
}
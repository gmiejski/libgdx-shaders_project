attribute vec3 a_position;
attribute vec4 a_color;

uniform mat4 u_projViewTrans;

varying vec4 vColor;

void main() {
    vColor = vec4(1.0f);
    gl_Position = u_projViewTrans * vec4(a_position.xyz , 1.0);
}
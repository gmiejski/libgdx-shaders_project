uniform mat4 u_projViewTrans;

attribute vec3 a_position;
attribute vec3 a_color;

uniform mat4 u_moveMatrix;
uniform mat4 u_rotationMatrix;

varying vec4 vColor;

void main() {
    vColor = vec4(a_color.xyz,0);
    gl_Position = u_projViewTrans * u_moveMatrix * u_rotationMatrix * vec4(a_position, 1.0f);
}
#ifdef GL_ES 
precision mediump float;
#endif

uniform sampler2D u_texture;

varying vec2 v_texCoord0;

uniform float u_fadeOut;

void main() {
    gl_FragColor = texture2D(u_texture, v_texCoord0);
    gl_FragColor.x = 1;
    gl_FragColor.a = gl_FragColor.z*u_fadeOut;
}